<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\Auth\AuthController;
use App\Http\Controllers\API\Account\MeController;
use App\Http\Controllers\API\Modules\JournalController;
use App\Http\Controllers\API\Modules\StudentController;
use App\Http\Controllers\API\Modules\EmployeeController;
use App\Http\Controllers\API\Modules\PresenceController;
use App\Http\Controllers\API\Modules\ClassroomController;
use App\Http\Controllers\API\Modules\DashboardController;
use App\Http\Controllers\API\Modules\Presence\BKPresence;
use App\Http\Controllers\API\Modules\StudentClassController;
use App\Http\Controllers\API\Modules\AcademicPeriodeController;
use App\Http\Controllers\API\Modules\Presence\EmployeePresence;
use App\Http\Controllers\API\Modules\Presence\StudentPresence;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::group(['prefix' => ''], function () {
    Route::post('login', [AuthController::class, 'login']);
    Route::get('refresh', [AuthController::class, 'refresh']);
    Route::get('me', [AuthController::class, 'me'])->middleware('jwt.auth');
    Route::post('reset-password', [AuthController::class, 'resetPassword'])->middleware('jwt.auth');
    Route::get('logout', [AuthController::class, 'logOut'])->middleware('jwt.auth');
});

Route::group(['prefix' => 'dashboard'], function () {
    Route::get('admin', [DashboardController::class, 'adminData']);
    Route::get('employee', [DashboardController::class, 'employeeData']);
    Route::get('student', [DashboardController::class, 'studentData']);
    Route::get('bk', [DashboardController::class, 'bkData']);
});

Route::group(['prefix' => 'academic_periode'], function () {
    Route::get('', [AcademicPeriodeController::class, 'getAcademicPeriode']);
    Route::post('store', [AcademicPeriodeController::class, 'storeAcademicPeriode']);
    Route::get('activation/{id}', [AcademicPeriodeController::class, 'activation']);
    Route::get('destroy/{id}', [AcademicPeriodeController::class, 'destroyAcademicPeriode']);
});

Route::group(['prefix' => 'student'], function () {
    Route::get('', [StudentController::class, 'getStudentAll']);
    Route::get('{id}', [StudentController::class, 'getStudentDetail']);
    Route::post('store', [StudentController::class, 'storeStudent']);
    Route::post('update/{id}', [StudentController::class, 'updateStudent']);
});

Route::group(['prefix' => 'employee'], function () {
    Route::get('', [EmployeeController::class, 'getEmployeeAll']);
    Route::get('{id}', [EmployeeController::class, 'getEmployeeDetail']);
    Route::post('store', [EmployeeController::class, 'storeEmployee']);
    Route::post('update/{id}', [EmployeeController::class, 'updateEmployee']);
});

Route::group(['prefix' => 'classroom'], function () {
    Route::get('', [ClassroomController::class, 'getClassroom']);
    Route::get('{id}', [ClassroomController::class, 'detailClassroom']);
    Route::post('store', [ClassroomController::class, 'storeClassroom']);
    Route::post('update/{id}', [ClassroomController::class, 'updateClassroom']);
});

Route::group(['prefix' => 'student-class'], function () {
    Route::get('', [StudentClassController::class, 'getStudentClass']);
    Route::get('mutation', [StudentClassController::class, 'studentMutation']);
    Route::post('mutation/store', [StudentClassController::class, 'studentMutationStore']);
});

Route::group(['prefix' => 'journal'], function () {
    Route::get('', [JournalController::class, 'getJournals']);
    Route::get('{id}', [JournalController::class, 'detail']);
    Route::post('store', [JournalController::class, 'store']);
    Route::get('presence/{id}', [JournalController::class, 'presence']);
});

Route::group(['prefix' => 'presence'], function () {
    Route::group(['prefix' => 'employee'], function () {
        Route::get('', [EmployeePresence::class, 'getPresence']);
        Route::get('{id}', [EmployeePresence::class, 'detailPresence']);
    });

    Route::group(['prefix' => 'bk'], function () {
        Route::get('', [BKPresence::class, 'getPresence']);
        Route::get('{id}', [BKPresence::class, 'detailPresence']);
        Route::post('update/{id}', [BKPresence::class, 'updatePresence']);
    });

    Route::group(['prefix' => 'student'], function () {
        Route::get('', [StudentPresence::class, 'getPresence']);
        Route::get('{id}', [StudentPresence::class, 'detailPresence']);
    });
});
