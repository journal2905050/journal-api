<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class AcademicPeriode extends Model
{
    use HasFactory;

    protected $guarded = [];

    protected $cats = [
        'is_active' => 'boolean',
    ];
}
