<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Employee extends Model
{
    use HasFactory, HasUuids;

    protected $guarded = [];

    protected $appends = [
        'people',
    ];

    public function getPeopleAttribute()
    {
        return People::where('id', $this->attributes['id'])->select(['name', 'gender'])->first();
    }
}
