<?php

namespace App\Http\Controllers\API\Service;

use App\Models\Presence;
use App\Models\StudentClass;
use Illuminate\Http\Request;
use App\Models\PresenceDaily;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class PresenceService extends Controller
{
    public static function trigerClass($class, $date)
    {
        DB::beginTransaction();
        try {
            $students = StudentClass::where('classroom', $class)->get()->pluck('student')->toArray();
            PresenceDaily::where('date', $date)->whereIn('student', $students)->delete();
            $payload = Presence::where('date', $date)->whereIn('student', $students)
                ->select([
                    'student',
                    'date',
                    DB::raw("count(presences.type = 'M' or null) as m"),
                    DB::raw("count(presences.type = 'I' or null) as i"),
                    DB::raw("count(presences.type = 'T' or null) as t"),
                    DB::raw("count(presences.type = 'A' or null) as a"),
                    DB::raw("count(presences.type in('M','I','T') or null) as presence"),
                    DB::raw("case count(presences.type in('M','I','T') or null)::dec when 0 then 100::dec else round(((count(presences.type in('M','I','T') or null)::dec/count(presences.type in('M','I','T','A') or null)::dec) * 100::dec),2) end as presentage"),
                ])
                ->groupBy([
                    'student',
                    'date'
                ]);
            PresenceDaily::insertUsing([
                'student',
                'date',
                'm',
                'i',
                't',
                'a',
                'presence',
                'presentage',
            ], $payload);
            DB::commit();
        } catch (\Throwable $th) {
            DB::rollBack();
            dd($th);
        }

        return true;
    }


    public static function trigerStudent($date, $students)
    {
        DB::beginTransaction();
        try {
            PresenceDaily::where('date', $date)->whereIn('student', $students)->delete();
            $payload = Presence::where('date', $date)->whereIn('student', $students)
                ->select([
                    'student',
                    'date',
                    DB::raw("count(presences.type = 'M' or null) as m"),
                    DB::raw("count(presences.type = 'I' or null) as i"),
                    DB::raw("count(presences.type = 'T' or null) as t"),
                    DB::raw("count(presences.type = 'A' or null) as a"),
                    DB::raw("count(presences.type in('M','I','T') or null) as presence"),
                    DB::raw("round(((count(presences.type in('M','I','T') or null)::dec/count(presences.type in('M','I','T','A') or null)::dec) * 100::dec),2) as presentage"),
                ])
                ->groupBy([
                    'student',
                    'date'
                ]);
            PresenceDaily::insertUsing([
                'student',
                'date',
                'm',
                'i',
                't',
                'a',
                'presence',
                'presentage',
            ], $payload);
            DB::commit();
        } catch (\Throwable $th) {
            DB::rollBack();
            dd($th);
        }

        return true;
    }
}
