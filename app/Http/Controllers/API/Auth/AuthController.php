<?php

namespace App\Http\Controllers\API\Auth;

use App\Models\User;
use App\Models\People;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\API\Helper\Response;
use Tymon\JWTAuth\Exceptions\UserNotDefinedException;

class AuthController extends Controller
{
    function __construct()
    {
        $this->middleware(['api']);
    }

    //login
    public function login(Request $request)
    {
        $request->validate([
            'username' => 'required',
            'password' => 'required',
        ]);

        try {
            // Search for users based on username
            $user = User::join('people','users.id', 'people.id')->where('username', $request->username)->first();

            // If the user is not found, provide an error response
            if (!$user) {
                return Response::fail([
                    'message' => 'Incorrect username or password, contact admin if not recorded',
                ]);
            }

            // Try authenticating with JWTAuth
            $token = JWTAuth::attempt([
                'username' => $request->username,
                'password' => $request->password,
            ]);
        } catch (UserNotDefinedException $e) {
            return Response::fail([
                'message' => $e->getMessage(),
            ]);
        }

        // If authentication is successful, send a success response
        return Response::success([
            'data' => [
                'name' => $user->people->name,
                'role' => [
                    'is_student' => $user->is_student,
                    'is_employee' => $user->is_employee,
                    'is_bk' => $user->is_bk,
                    'is_admin' => $user->is_admin,
                ],
                'token_type' => 'bearer',
                'expires_in' => JWTAuth::factory()->getTTL() * 60,
                'access_token' => $token,
            ]
        ]);
    }


    public function refresh()
    {
        $token = JWTAuth::getToken();

        if ($token) {
            $newToken = JWTAuth::refresh($token);

            return Response::success([
                'data' => [
                    'token_type' => 'bearer',
                    'expires_in' => JWTAuth::factory()->getTTL() * 60,
                    'access_token' => $newToken,
                ]
            ]);
        } else {
            return Response::fail(['message' => 'A token is required'], 401);
        }
    }


    public function me(Request $request)
    {
        $people = People::join('users', 'people.id', 'users.id')
            ->where('people.id', $request->user()->id)
            ->select([
                'users.id',
                'users.username',
                'people.name',
                'people.gender',
                'users.is_student',
                'users.is_employee',
                'users.is_bk',
                'users.is_admin'
            ])->first();
        return Response::success([
            'data' => $people,
        ]);
    }


    public function resetPassword(Request $request)
    {
        $request->validate([
            'password' => ['required'],
            'password_new' => ['required'],
            'confirmed_password' => ['required', 'same:password_new'],
        ]);

        $confirmed_password = $request->confirmed_password;
        if (JWTAuth::check($confirmed_password, JWTAuth::user()->password)) {
            return Response::fail([
                'message' => 'The current password is inappropriate'
            ]);
        }

        User::where('id', $request->user()->id)->update([
            'password' => Hash::make($request->password_new)
        ]);
        JWTAuth::invalidate(JWTAuth::getToken());
        return Response::success();
    }


    public function logOut()
    {
        JWTAuth::invalidate(JWTAuth::getToken());
        return Response::success(['message' => 'Successfully logged out']);
    }
}
