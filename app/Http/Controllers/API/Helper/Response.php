<?php

namespace App\Http\Controllers\API\Helper;

use App\Http\Controllers\Controller;

class Response extends Controller
{
    //
    public static function success($payload = [])
    {
        $payload = (object) $payload;
        return [
            'success' => true,
            'message' => $payload->message ?? 'Success',
            'data' => $payload->data ?? null
        ];
    }

    public static function fail($payload = [])
    {
        $payload = (object) $payload;
        return [
            'success' => false,
            'message' => $payload->message ?? 'Error',
            'data' => $payload->data ?? null
        ];
    }
}
