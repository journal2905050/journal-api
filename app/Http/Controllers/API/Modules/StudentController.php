<?php

namespace App\Http\Controllers\API\Modules;

use App\Models\User;
use App\Models\People;
use App\Models\Student;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\API\Helper\Response;

class StudentController extends Controller
{
    function __construct()
    {
        $this->middleware(['api', 'jwt.auth', 'admin'], ['except' => []]);
    }

    public function getStudentAll(Request $request)
    {
        $limit = $request->input('limit',10);
        $model = People::query()
            ->join('students', 'people.id', 'students.id');

        $data = $model->select([
            'people.id',
            'students.nis',
            'people.name',
            'people.gender',
            'students.batch_in'
        ])->paginate($limit);

        return Response::success([
            'data' => $data
        ]);
    }

    public function getStudentDetail($id)
    {
        $student = People::query()
            ->join('students', 'people.id', 'students.id')
            ->select([
                'people.id',
                'students.nis',
                'people.name',
                'people.gender',
                'students.batch_in'
            ])->find($id);

        return Response::success([
            'data' => $student
        ]);
    }

    public function storeStudent(Request $request)
    {
        $request->validate([
            'name' => ['required'],
            'nis' => ['required', "unique:users,username"],
            'gender' => ['required'],
            'password' => ['required'],
            'batch_in' => ['required'],
        ]);

        $nis = $request->nis;

        $people = People::create([
            'name' => $request->name,
            'gender' => $request->gender,
        ]);

        User::create([
            'id' => $people->id,
            'username' => $nis,
            'password' => Hash::make($request->password),
            'is_student' => true,
        ]);

        Student::create([
            'id' => $people->id,
            'nis' => $nis,
            'batch_in' => $request->batch_in,
        ]);

        return Response::success();
    }

    public function updateStudent(Request $request, $id)
    {
        $request->validate([
            'name' => ['required'],
            'nis' => ['required', Rule::unique('users', 'username')->ignore($id)],
            'gender' => ['required'],
            'password' => ['required'],
            'batch_in' => ['required'],
        ]);

        $people = People::find($id);
        $user = User::find($id);
        $student = Student::find($id);

        if (!$people || !$user || !$student) {
            // Handle if data not found
            return Response::fail(['message' => 'Data not found']);
        }

        $nis = $request->nis;
        $people->update([
            'name' => $request->name,
            'gender' => $request->gender,
        ]);

        $user->update([
            'username' => $nis,
            'password' => Hash::make($request->password),
            'is_student' => true,
        ]);

        $student->update([
            'nis' => $nis,
            'batch_in' => $request->batch_in,
        ]);

        return Response::success();
    }
}
