<?php

namespace App\Http\Controllers\API\Modules;

use App\Models\User;
use App\Models\People;
use App\Models\Employee;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;
use App\Http\Controllers\API\Helper\Response;

class EmployeeController extends Controller
{
    function __construct()
    {
        $this->middleware(['api', 'jwt.auth', 'admin'], ['except' => []]);
    }


    public function getEmployeeAll(Request  $request)
    {
        $limit = $request->input('limit', 10);
        $model = People::query()
            ->join('employees', 'people.id', 'employees.id');

        // if ($request->input('search', false)) {
        //     $model = $model->where('people.name', $request->search);
        //     $model = $model->where('employees.nip', $request->search);
        // }

        $data = $model->select([
            'people.id',
            'people.name',
            'employees.nip',
            'employees.alias',
            'employees.rank',
            'employees.position',
            'employees.status',
        ])->paginate($limit);

        return Response::success([
            'data' => $data
        ]);
    }


    public function getEmployeeDetail($id)
    {
        $employee = People::join('employees', 'people.id', 'employees.id')
            ->select([
                'people.id',
                'people.name',
                'employees.nip',
                'employees.alias',
                'employees.rank',
                'employees.position',
                'employees.status',
            ])->find($id);

        return Response::success([
            'data' => $employee
        ]);
    }


    public function storeEmployee(Request $request)
    {
        $request->validate([
            "name" => ['required'],
            "nip" => ['nullable', 'unique:employees,nip'],
            "username" => ['required', 'unique:users,username'],
            "alias" => ['nullable', 'unique:employees,alias'],
            "rank" => ['nullable'],
            "is_admin" => ['boolean', 'nullable'],
            "is_employee" => ['boolean', 'nullable'],
            "is_bk" => ['boolean', 'nullable'],
            "position" => ['nullable'],
            "status" => ['nullable'],
            "gender" => ['required'],
            "password" => ['required'],
        ]);

        try {
            $people = People::create([
                "name" => $request->name,
                "gender" => $request->gender,
            ]);

            User::create([
                'id' => $people->id,
                "username" => $request->username,
                "password" => $request->password,
                "is_admin" => $request->is_admin,
                "is_bk" => $request->is_bk,
                "is_employee" => $request->is_employee,
            ]);

            Employee::create([
                'id' => $people->id,
                "alias" => $request->alias,
                "nip" => $request->nip,
                "rank" => $request->rank,
                "status" => $request->status,
                "position" => $request->position,
            ]);

            return Response::success();
        } catch (\Exception $e) {
            return Response::fail([
                'message' => $e->getMessage(),
            ]);
        }
    }


    public function updateEmployee(Request $request, $id)
    {
        $request->validate([
            "name" => ['required'],
            "nip" => ['nullable', Rule::unique('employees', 'nip')->ignore($id)],
            "alias" => ['nullable', Rule::unique('employees', 'alias')->ignore($id)],
            "rank" => ['nullable'],
            "username" => ['required', Rule::unique('users', 'username')->ignore($id)],
            "position" => ['nullable'],
            "is_admin" => ['boolean', 'nullable'],
            "is_employee" => ['boolean', 'nullable'],
            "is_bk" => ['boolean', 'nullable'],
            "status" => ['nullable'],
            "gender" => ['required'],
            "password" => ['required'],
        ]);

        $people = People::find($id);
        $user = User::find($id);
        $employee = Employee::find($id);

        if (!$people || !$user || !$employee) {
            // Handle if data not found
            return Response::fail(['message' => 'Data not found']);
        }

        try {
            $people->update([
                "name" => $request->name,
                "gender" => $request->gender,
            ]);

            $user->update([
                "username" => $request->username,
                "password" => $request->password,
                "is_admin" => $request->is_admin,
                "is_bk" => $request->is_bk,
                "is_employee" => $request->is_employee,
            ]);

            $employee->update([
                "alias" => $request->alias,
                "nip" => $request->nip,
                "rank" => $request->rank,
                "status" => $request->status,
                "position" => $request->position,
            ]);

            return Response::success();
        } catch (\Exception $e) {
            return Response::fail([
                'message' => $e->getMessage(),
            ]);
        }
    }
}
