<?php

namespace App\Http\Controllers\API\Modules;

use Exception;
use App\Models\Journal;
use App\Models\Employee;
use App\Models\Presence;
use App\Models\Classroom;
use App\Models\JournalHour;
use App\Models\StudentClass;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\API\Helper\Response;
use App\Http\Controllers\API\Service\PresenceService;
use Carbon\Carbon;

class JournalController extends Controller
{
    function __construct()
    {
        $this->middleware(['api', 'jwt.auth', 'employee'], ['except' => []]);
    }


    public function getJournals(Request $request)
    {
        try {
            $limit = $request->input('limit',10);
            $model = Journal::join('journal_hours', 'journals.id', 'journal_hours.journal')
                ->join('classrooms', 'journals.classroom', 'classrooms.id')
                ->where([
                    'journals.employee'=> $request->user()->id,
                    'journals.date' => $request->input('date', Carbon::now()->format('Y-m-d')),
                ]);

            $journals = $model->select([
                'journals.id',
                'classrooms.name as class_name',
                'classrooms.grade',
            ])->selectRaw('STRING_AGG(CAST(journal_hours.hour AS VARCHAR), \',\') as hours_to')
                ->groupBy('journals.id', 'classrooms.name', 'classrooms.grade')
                ->paginate($limit);

            if ($journals->isEmpty()) {
                // No journals found, return an appropriate response
                return Response::success([
                    'message' => 'No journals found for the user.',
                ]);
            }

            return Response::success([
                'data' => $journals
            ]);
        } catch (Exception $e) {
            // Handle the exception if no records are found
            return Response::fail([
                'message' => $e->getMessage(),
            ]);
        }
    }


    public function detail($id)
    {
        $journal = Journal::where('id', $id)->first();
        $journal_hours = JournalHour::where('journal', $id)->get();
        $classroom = Classroom::where('id', $journal->classroom)->first();
        $studentClasses = StudentClass::join('students', 'student_classes.student', 'students.id')
            ->join('people', 'student_classes.student', 'people.id')
            ->where('classroom', $journal->classroom)
            ->select([
                'students.id',
                'students.nis',
                'people.name',
            ])->get();

        $studentClassData = [];
        foreach ($studentClasses as $studentClass) {
            // Reset presenceData for each student
            $presenceData = [];

            foreach ($journal_hours as $journal_hour) {
                $presenceData[] = [
                    'type' => Presence::where([
                        'student' => $studentClass->id,
                        'journal' => $journal_hour->journal,
                        'hour' => $journal_hour->hour
                    ])->first()->type ?? 'X',
                ];
            }

            $studentClassData[] = [
                'id' => $studentClass->id,
                'nis' => $studentClass->nis,
                'name' => $studentClass->name,
                'presence' => $presenceData
            ];
        }


        $data = [
            'id' => $journal->id,
            'classroom' => $classroom->name,
            'grade' => $classroom->grade,
            'date' => $journal->date,
            'school_year' => $classroom->school_year,
            'hours' => $journal_hours->pluck('hour')->toArray(),
            'hours_label' => implode(',', $journal_hours->pluck('hour')->toArray()),
            'subject' => $journal->subject,
            'summary' => $journal->summary,
            'students' => $studentClassData,
        ];

        return Response::success([
            'data' => $data
        ]);
    }


    public function store(Request $request)
    {
        $employee = Employee::where('id', $request->user()->id)->first();
        if (!$employee) {
            return Response::fail(['message' => 'Not have access']);
        }

        try {
            $request->validate([
                'date' => ['required', 'date'],
                'grade' => ['required'],
                'classroom' => ['required'],
                'subject' => ['required'],
                // 'school_year' => ['required'],
                'hours' => ['required', 'array', 'min:1'],
            ]);

            $class = Classroom::where([
                'grade' => $request->grade,
                'name' => $request->classroom
            ])->first();

            if (!$class) {
                return Response::fail([
                    'message' => 'Classroom does not exist'
                ]);
            }

            $journal = Journal::create([
                'date' => $request->date,
                'employee' => $request->user()->id,
                'classroom' => $class->id,
                'subject' => $request->subject,
                'summary' => $request->summary ?? null
            ]);

            foreach ($request->hours as $hour) {
                JournalHour::create([
                    'journal' => $journal->id,
                    'hour' => $hour
                ]);
            }

            return Response::success();
        } catch (\Exception $e) {
            return Response::fail([
                'message' => $e->getMessage(),
            ]);
        }
    }


    public function presence(Request $request, $id)
    {
        try {
            $request->validate([
                'presence' => ['required', 'array'],
            ]);

            $journal = Journal::where('id', $id)->first();

            Journal::where('id', $id)->update([
                'summary' => $request->summary
            ]);

            Presence::where('journal', $id)->delete();
            Presence::insert($request->presence);
            PresenceService::trigerClass($journal->classroom, $request->date);

            return Response::success();
        } catch (\Exception $e) {
            return Response::fail([
                'message' => $e->getMessage(),
            ]);
        }
    }
}
