<?php

namespace App\Http\Controllers\API\Modules;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PeopleController extends Controller
{
    function __construct()
    {
        $this->middleware(['api', 'jwt.auth'],['except' => []]);
    }
}
