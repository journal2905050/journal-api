<?php

namespace App\Http\Controllers\API\Modules;

use App\Models\Student;
use Illuminate\Support\Str;
use App\Models\StudentClass;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\API\Helper\Response;

class StudentClassController extends Controller
{
    function __construct()
    {
        $this->middleware(['api', 'jwt.auth', 'admin'], ['except' => []]);
    }

    public function getStudentClass(Request $request)
    {
        $limit = $request->input('limit',10);
        $model = StudentClass::query()
            ->join('classrooms', 'student_classes.classroom', 'classrooms.id')
            ->join('people', 'student_classes.student', 'people.id')
            ->join('students', 'student_classes.student', 'students.id');

        if ($request->input('school_year', false)) {
            $model = $model->where('classrooms.school_year', $request->school_year);
        }

        if ($request->input('grade', false)) {
            $model = $model->where('classrooms.grade', $request->grade);
        }

        if ($request->input('classroom', false)) {
            $model = $model->where('classrooms.name', $request->classroom);
        }

        $results = $model->select([
            'student_classes.id',
            'students.nis',
            'people.name',
            'classrooms.name as classroom',
            'classrooms.grade',
            'classrooms.school_year',
        ])->paginate($limit);

        return Response::success([
            'data' => $results
        ]);
    }


    public function studentMutation(Request $request)
    {
        $request->validate([
            'school_year' => ['required', 'exists:academic_periodes,school_year'],
            'batch_in' => ['required'],
        ]);

        $limit = $request->input('limit',10);
        $model = Student::join('people', 'students.id', 'people.id')
            ->leftJoin('student_classes', function ($qons) use ($request) {
                $qons->on('students.id', 'student_classes.student');
            })
            ->leftJoin('classrooms', function ($qonc) use ($request) {
                $qonc->on('student_classes.classroom', 'classrooms.id')
                    ->where('classrooms.school_year', $request->school_year);
            })
            ->where('students.batch_in', $request->batch_in);

        $data = $model->select([
            'students.id',
            'students.nis',
            'people.name',
            'classrooms.name as classroom'
        ])->paginate($limit);

        return Response::success([
            'data' => $data,
        ]);
    }


    public function studentMutationStore(Request $request)
    {
        $request->validate([
            'student' => ['array'],
            'classroom' => ['required'],
            'school_year' => ['required', 'exists:academic_periodes,school_year'],
        ]);

        $classroom = $request->classroom;
        $students = $request->input('students', []);
        StudentClass::join('classrooms', 'student_classes.classroom', 'classrooms.id')
            ->whereIn('student_classes.student', $students)
            ->where('classrooms.school_year', $request->school_year)
            ->delete();

        $mutation = [];
        foreach ($request->students ?? [] as $value) {
            $mutation[] = [
                'id' => Str::uuid(),
                'student' => $value,
                'classroom' => $classroom
            ];
        }
        StudentClass::insert($mutation);

        return Response::success();
    }
}
