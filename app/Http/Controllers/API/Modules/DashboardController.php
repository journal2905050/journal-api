<?php

namespace App\Http\Controllers\API\Modules;

use Carbon\Carbon;
use App\Models\People;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use App\Http\Controllers\Controller;
use App\Http\Controllers\API\Helper\Response;
use App\Models\Journal;

class DashboardController extends Controller
{
    function __construct()
    {
        $this->middleware(['api', 'jwt.auth'], ['except' => []]);
    }


    //Dashboard Admin
    public function adminData()
    {
        $data = [
            'total' => People::count(),
            'total_student' => People::join('users', 'people.id', 'users.id')->where('users.is_student', true)->count(),
            'total_employee' => People::join('users', 'people.id', 'users.id')->where('users.is_employee', true)->count(),
            'total_bk' => People::join('users', 'people.id', 'users.id')->where('users.is_bk', true)->count(),
            'total_admin' => People::join('users', 'people.id', 'users.id')->where('users.is_bk', true)->count(),

            'total_student_boy' => People::join('users', 'people.id', 'users.id')->where('users.is_student', true)->where('people.gender', 'L')->count(),
            'total_student_girl' => People::join('users', 'people.id', 'users.id')->where('users.is_student', true)->where('people.gender', 'P')->count(),

            'total_employee_man' => People::join('users', 'people.id', 'users.id')->where('users.is_employee', true)->where('people.gender', 'L')->count(),
            'total_employee_women' => People::join('users', 'people.id', 'users.id')->where('users.is_employee', true)->where('people.gender', 'P')->count(),

            'total_bk_man' => People::join('users', 'people.id', 'users.id')->where('users.is_bk', true)->where('people.gender', 'L')->count(),
            'total_bk_women' => People::join('users', 'people.id', 'users.id')->where('users.is_bk', true)->where('people.gender', 'P')->count(),
        ];

        return Response::success([
            'data' => $data
        ]);
    }


    //Dashboard Employee
    function getCountByTypeEmployee($type, $user_id, $date)
    {
        return Journal::join('presences', 'journals.id', 'presences.journal')
            ->where([
                'journals.employee' => $user_id,
                'presences.date' => $date
            ])
            ->when($type !== null, fn ($query) => $query->where('presences.type', $type))
            ->count();
    }


    public function employeeData(Request $request)
    {
        App::setLocale('id');
        $dateNow = Carbon::now('Asia/Jakarta');
        $user_id = $request->user()->id;

        $enter = $this->getCountByTypeEmployee('M', $user_id, $dateNow->toDateString());
        $permission = $this->getCountByTypeEmployee('I', $user_id, $dateNow->toDateString());
        $late = $this->getCountByTypeEmployee('T', $user_id, $dateNow->toDateString());
        $alpha = $this->getCountByTypeEmployee('A', $user_id, $dateNow->toDateString());

        // calculates the total only if the type value is not null
        $total = array_sum([$enter, $permission, $late, $alpha]);

        $result = [
            'dayNow' => $dateNow->translatedFormat('l'),
            'dateNow' => $dateNow->toDateString(),
            'enter' => $enter,
            'permission' => $permission,
            'late' => $late,
            'alpha' => $alpha,
            'total' => $total
        ];

        return Response::success([
            'data' => $result
        ]);
    }


    //Dashboard Student
    function getCountByTypeStudent($type, $user_id, $date)
    {
        return Journal::join('presences', 'journals.id', 'presences.journal')
            ->where([
                'presences.student' => $user_id,
                'presences.date' => $date
            ])
            ->when($type !== null, fn ($query) => $query->where('presences.type', $type))
            ->count();
    }


    public function studentData(Request $request)
    {
        App::setLocale('id');
        $dateNow = Carbon::now('Asia/Jakarta');
        $user_id = $request->user()->id;

        $enter = $this->getCountByTypeStudent('M', $user_id, $dateNow->toDateString());
        $permission = $this->getCountByTypeStudent('I', $user_id, $dateNow->toDateString());
        $late = $this->getCountByTypeStudent('T', $user_id, $dateNow->toDateString());
        $alpha = $this->getCountByTypeStudent('A', $user_id, $dateNow->toDateString());

        // calculates the total only if the type value is not null
        $total = array_sum([$enter, $permission, $late, $alpha]);

        $result = [
            'dayNow' => $dateNow->translatedFormat('l'),
            'dateNow' => $dateNow->toDateString(),
            'enter' => $enter,
            'permission' => $permission,
            'late' => $late,
            'alpha' => $alpha,
            'total' => $total
        ];

        return Response::success([
            'data' => $result
        ]);
    }


    //Dashboard Bk
    function getCountByTypeBk($type, $date)
    {
        return Journal::join('presences', 'journals.id', 'presences.journal')
            ->where('presences.date', $date)
            ->when($type !== null, fn ($query) => $query->where('presences.type', $type))
            ->count();
    }


    public function bkData(Request $request)
    {
        App::setLocale('id');
        $dateNow = Carbon::now('Asia/Jakarta');
        $user_id = $request->user()->id;

        $enter = $this->getCountByTypeBk('M',  $dateNow->toDateString());
        $permission = $this->getCountByTypeBk('I',  $dateNow->toDateString());
        $late = $this->getCountByTypeBk('T',  $dateNow->toDateString());
        $alpha = $this->getCountByTypeBk('A',  $dateNow->toDateString());

        // calculates the total only if the type value is not null
        $total = array_sum([$enter, $permission, $late, $alpha]);

        $result = [
            'dayNow' => $dateNow->translatedFormat('l'),
            'dateNow' => $dateNow->toDateString(),
            'enter' => $enter,
            'permission' => $permission,
            'late' => $late,
            'alpha' => $alpha,
            'total' => $total
        ];

        return Response::success([
            'data' => $result
        ]);
    }
}
