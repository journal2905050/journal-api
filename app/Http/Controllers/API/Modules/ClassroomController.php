<?php

namespace App\Http\Controllers\API\Modules;

use App\Models\Classroom;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\API\Helper\Response;

class ClassroomController extends Controller
{
    function __construct()
    {
        $this->middleware(['api', 'jwt.auth', 'admin'], ['except' => []]);
    }


    public function getClassroom(Request $request)
    {
        $limit = $request->input('limit',10);
        $model = Classroom::query();

        if ($request->input('school_year',false)){
            $model = $model->where('school_year', $request->school_year);
        }

        if ($request->input('grade',false)){
            $model = $model->where('grade', $request->grade);
        }

        $data = $model->select([
            'classrooms.id',
            'classrooms.school_year',
            'classrooms.grade',
            'classrooms.name'
        ])->paginate($limit);

        return Response::success([
            'data' => $data
        ]);
    }


    public function detailClassroom($id)
    {
        $classrooms = Classroom::select([
            'classrooms.id',
            'classrooms.school_year',
            'classrooms.grade',
            'classrooms.name'
        ])->find($id);

        return Response::success([
            'data' => $classrooms
        ]);
    }


    public function storeClassroom(Request $request)
    {
        $request->validate([
            'school_year' => ['required', 'exists:academic_periodes,school_year'],
            'grade' => ['required'],
            'name' => ['required']
        ]);

        Classroom::create($request->only([
            'school_year', 'grade', 'name'
        ]));

        return Response::success();
    }


    public function updateClassroom(Request $request, $id)
    {
        $request->validate([
            'school_year' => ['required', 'exists:academic_periodes,school_year'],
            'grade' => ['required'],
            'name' => ['required']
        ]);

        Classroom::where('id', $id)->update($request->only([
            'school_year', 'grade', 'name'
        ]));

        return Response::success();
    }
}
