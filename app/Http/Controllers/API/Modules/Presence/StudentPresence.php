<?php

namespace App\Http\Controllers\API\Modules\Presence;

use Carbon\Carbon;
use App\Models\People;
use App\Models\Presence;
use Carbon\CarbonPeriod;
use Illuminate\Http\Request;
use App\Models\PresenceDaily;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Controllers\API\Helper\Response;

class StudentPresence extends Controller
{
    function __construct()
    {
        $this->middleware(['api', 'jwt.auth', 'student'], ['except' => []]);
    }

    public function getPresence(Request $request)
    {
        $month = $request->input('month', Carbon::now()->format('Y-m'));

        $startDate = Carbon::createFromFormat('Y-m', $month)->startOfMonth();
        $endDate = Carbon::createFromFormat('Y-m', $month)->endOfMonth();

        $period = CarbonPeriod::create($startDate, $endDate);

        $user = $request->user();
        $dateList = [];

        foreach ($period as $date) {
            $presence = PresenceDaily::where([
                'date' => $date,
                'student' => $user->id
            ])->select([
                'id',
                DB::raw('round(m) as m'),
                DB::raw('round(i) as i'),
                DB::raw('round(t) as t'),
                DB::raw('round(a) as a'),
                DB::raw('round(presentage) as presentage'),
            ])->first();

            $dateList[] = [
                'date' => $date->toDateString(),
                'enter' => $presence->m ?? 0,
                'permission' => $presence->i ?? 0,
                'late' => $presence->t ?? 0,
                'alpha' => $presence->a ?? 0,
                'presentage' => $presence->presentage ?? ' - ' . ' %',
            ];
        }

        return Response::success([
            'data' => $dateList
        ]);
    }


    public function detailPresence(Request $request)
    {
        $people = People::join('students', 'people.id', 'students.id')
            ->select([
                'people.id',
                'people.name',
                'students.nis'
            ])->where('people.id', $request->user()->id)
            ->first();

        $presence = Presence::join('journals', 'presences.journal', 'journals.id')
            ->join('people', 'journals.employee', 'people.id')
            ->join('classrooms', 'journals.classroom', 'classrooms.id')
            ->select([
                'presences.id',
                'presences.hour',
                DB::raw("coalesce(presences.type,'X') as type"),
                'people.name',
                'journals.subject',
                'classrooms.name as classroom'
            ])->where([
                'presences.student' => $request->user()->id,
                'presences.date' => $request->date
            ])->get();

        return Response::success([
            'data' => [
                'id' => $people->id,
                'name' => $people->name,
                'nis' => $people->nis,
                'date' => $request->date,
                'presence' => $presence
            ]
        ]);
    }

}
