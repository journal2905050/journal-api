<?php

namespace App\Http\Controllers\API\Modules\Presence;

use App\Models\People;
use App\Models\Presence;
use App\Models\StudentClass;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Controllers\API\Helper\Response;

class EmployeePresence extends Controller
{

    function __construct()
    {
        $this->middleware(['api', 'jwt.auth', 'employee'],['except' => []]);
    }

    public function getPresence(Request $request)
    {
        $classroom = $request->classroom;
        $date = $request->date;

        if (!$classroom || !$date) {
            return Response::fail([
                'message' => 'Please provide a classroom and date'
            ]);
        }

        $student_classes = StudentClass::join('students', 'student_classes.student', 'students.id')
            ->join('people', 'student_classes.student', 'people.id')
            ->where('student_classes.classroom', $classroom)
            ->leftJoin('presence_dailies', function ($qpd) use ($date) {
                $qpd->on('students.id', 'presence_dailies.student')
                    ->where('presence_dailies.date', $date);
            })
            ->select([
                'students.id',
                'students.nis',
                'people.name',
                DB::raw("round(coalesce(presence_dailies.presentage,null)) as presentage")
            ])->get();


        $studentClassData = [];
        foreach ($student_classes as $studentClass) {
            // Reset presenceData for each student
            $presenceData = [];

            for ($i = 1; $i <= 12; $i++) {
                $presenceData[] = [
                    'type' => Presence::where([
                        'date' => $date,
                        'student' => $studentClass->id,
                        'hour' => $i
                    ])->first()->type ?? 'X',
                ];
            }

            $studentClassData[] = [
                'id' => $studentClass->id,
                'nis' => $studentClass->nis,
                'name' => $studentClass->name,
                'presentage' => $studentClass->presentage . '%' ?? '-',
                'presence' => $presenceData
            ];
        }


        return Response::success([
            'data' => $studentClassData
        ]);
    }


    public function detailPresence($id, Request $request)
    {
        $people = People::join('students', 'people.id', 'students.id')
            ->select([
                'people.id',
                'people.name',
                'students.nis'
            ])->where('people.id', $id)
            ->first();

        $presence = Presence::join('journals', 'presences.journal', 'journals.id')
            ->join('people', 'journals.employee', 'people.id')
            ->join('classrooms', 'journals.classroom', 'classrooms.id')
            ->select([
                'presences.id',
                'presences.hour',
                DB::raw("coalesce(presences.type,'X') as type"),
                'people.name',
                'journals.subject',
                'classrooms.name as classroom'
            ])->where([
                'presences.student' => $id,
                'presences.date' => $request->date
            ])->get();

        return Response::success([
            'data' => [
                'id' => $people->id,
                'name' => $people->name,
                'nis' => $people->nis,
                'date' => $request->date,
                'presence' => $presence
            ]
        ]);
    }
}
