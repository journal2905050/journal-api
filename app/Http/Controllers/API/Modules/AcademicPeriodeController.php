<?php

namespace App\Http\Controllers\API\Modules;

use Illuminate\Http\Request;
use App\Models\AcademicPeriode;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Controllers\API\Helper\Response;

class AcademicPeriodeController extends Controller
{
    function __construct()
    {
        $this->middleware(['api', 'jwt.auth', 'admin'], ['except' => []]);
    }


    public function setActive($id)
    {
        AcademicPeriode::whereNotNUll('id')->update(['is_active' => false]);
        AcademicPeriode::where('id', $id)->update(['is_active' => true]);

        return true;
    }


    public function getAcademicPeriode(Request $request)
    {
        $model = AcademicPeriode::query();

        $limit = $request->input('limit',10);
        if ($request->input('search', false)) {
            $model = $model->where('school_year', $request->search);
        }

        $data = $model->select([
            'academic_periodes.id',
            'academic_periodes.school_year',
            'academic_periodes.is_active'
        ])->paginate($limit);

        return Response::success([
            'data' => $data,
        ]);
    }


    public function storeAcademicPeriode(Request $request)
    {
        $request->validate([
            'school_year' => 'required|unique:academic_periodes,school_year',
            'is_active' => 'boolean',
        ]);

        $isActive = $request->has('is_active');
        $academicPeriode = AcademicPeriode::create([
            'school_year' => $request->school_year,
        ]);

        if ($isActive) {
            $this->setActive($academicPeriode->id);
        }

        return Response::success();
    }


    public function activation($id)
    {
        $ret = $this->setActive($id);
        if ($ret) {
            return Response::success();
        }
        return Response::fail();
    }


    public function destroyAcademicPeriode($id)
    {
        DB::beginTransaction();
        try {
            AcademicPeriode::where('id', $id)->delete();
            DB::commit();
            return Response::success();
        } catch (\Throwable $th) {
            DB::rollBack();
            return Response::fail([
                'message' => 'Data that has been used cannot be deleted.'
            ]);
        }
    }
}
