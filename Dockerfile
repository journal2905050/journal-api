FROM php:8.2.4-apache

ARG S6_OVERLAY_VERSION=3.1.2.1

USER root

WORKDIR /var/www/html

RUN apt update && apt install -y \
    libpng-dev \
    zlib1g-dev \
    libxml2-dev \
    libzip-dev \
    libonig-dev \
    libpq-dev \
    zip \
    curl \
    unzip \
    && docker-php-ext-configure gd \
    && docker-php-ext-install -j$(nproc) gd \
    # && docker-php-ext-install pdo_mysql \
    && docker-php-ext-install pdo_pgsql \
    # && docker-php-ext-install mysqli \
    && docker-php-ext-install zip \
    && docker-php-source delete \
    && docker-php-ext-install calendar

COPY ./vhost.conf /etc/apache2/sites-available/000-default.conf
COPY . .
COPY .env.example .env
COPY rootfs/ /
RUN chmod +x /etc/services.d/apache2/run && chmod +x /etc/services.d/queue/run

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

RUN chown -R www-data:www-data /var/www/html && a2enmod rewrite

RUN composer install --no-cache

ADD https://github.com/just-containers/s6-overlay/releases/download/v${S6_OVERLAY_VERSION}/s6-overlay-noarch.tar.xz /tmp
RUN tar -C / -Jxpf /tmp/s6-overlay-noarch.tar.xz
ADD https://github.com/just-containers/s6-overlay/releases/download/v${S6_OVERLAY_VERSION}/s6-overlay-x86_64.tar.xz /tmp
RUN tar -C / -Jxpf /tmp/s6-overlay-x86_64.tar.xz
ENTRYPOINT ["/init"]