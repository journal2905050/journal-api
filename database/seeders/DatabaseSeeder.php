<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;

use Database\Seeders\Data\AcademicPeriodeSeeder;
use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Database\Seeders\Data\StudentSeeder;
use Database\Seeders\Data\AdminSeeder;
use Database\Seeders\Data\EmployeeSeeder;
use Database\Seeders\Data\ClassroomSeeder;
use Database\Seeders\Data\JournalSeeder;
use Database\Seeders\Data\PresenceSeeder;
use Database\Seeders\Data\StudentClassSeeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        // \App\Models\User::factory(10)->create();

        $this->call([
            AdminSeeder::class,
            AcademicPeriodeSeeder::class,
            StudentSeeder::class,
            EmployeeSeeder::class,
            ClassroomSeeder::class,
            StudentClassSeeder::class,
            JournalSeeder::class,
            // PresenceSeeder::class,
        ]);

        // User::create([
        //     'id' => '550e8400-e29b-41d4-a716-446655440000',
        //     'username' => 'admin@gmail.com',
        //     'password' => '12345678',
        //     'is_admin' => true,
        //     'people_id' => '450e8400-e29b-41d4-a716-446655440000'
        // ]);

        // People::create([
        //     'id' => '450e8400-e29b-41d4-a716-446655440000',
        //     'name' => 'Admin',
        // ]);

        // for ($i = 0; $i < 5; $i++) {
        //     User::create([
        //         'id' => Str::uuid(),
        //         'username' => mt_rand(10000, 99999),
        //         'password' => '12345678',
        //         'is_student' => true,
        //     ]);
        // }

        // for ($i = 0; $i < 3; $i++) {
        //     User::create([
        //         'id' => Str::uuid(),
        //         'username' => mt_rand(10000, 99999),
        //         'password' => '12345678',
        //         'is_teacher' => true,
        //     ]);
        // }

        // for ($i = 0; $i < 2; $i++) {
        //     User::create([
        //         'id' => Str::uuid(),
        //         'username' => mt_rand(10000, 99999),
        //         'password' => '12345678',
        //         'is_bk' => true,
        //     ]);
        // }
    }
}
