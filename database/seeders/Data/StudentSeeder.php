<?php

namespace Database\Seeders\Data;

use App\Models\User;
use App\Models\People;
use App\Models\Student;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class StudentSeeder extends Seeder
{
    /**+
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::beginTransaction();
        try {
            People::insert([
                [
                    'id' => '550e8400-e29b-41d4-a716-446655440001',
                    'name' => 'Aditya Prabowo Nugroho Saputra',
                    'gender' => 'L',
                ],
                [
                    'id' => '550e8400-e29b-41d4-a716-446655440004',
                    'name' => 'Rizki Muhammad Faisal Al-Haq',
                    'gender' => 'L',
                ]
            ]);

            User::insert([
                [
                    'id' => '550e8400-e29b-41d4-a716-446655440001',
                    'username' => '44025',
                    'password' => Hash::make('12345678'),
                    'is_student' => true,
                ],
                [
                    'id' => '550e8400-e29b-41d4-a716-446655440004',
                    'username' => '98765',
                    'password' => Hash::make('12345678'),
                    'is_student' => true,
                ]
            ]);

            Student::insert([
                [
                    'id' => '550e8400-e29b-41d4-a716-446655440001',
                    'nis' => '44025',
                    'batch_in' => '2023',
                ],
                [
                    'id' => '550e8400-e29b-41d4-a716-446655440004',
                    'nis' => '98765',
                    'batch_in' => '2023',
                ],
            ]);
            DB::commit();
        } catch (\Throwable $th) {
            DB::rollBack();
            print($th->getMessage());
        }
    }
}
