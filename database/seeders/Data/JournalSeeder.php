<?php

namespace Database\Seeders\Data;

use App\Models\Journal;
use App\Models\JournalHour;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class JournalSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::beginTransaction();
        try {
            Journal::insert([
                [
                    'id' => '550e8400-e29b-41d4-a716-446655440000',
                    'employee' => '550e8400-e29b-41d4-a716-446655440002',
                    'classroom' => '550e8400-e29b-41d4-a716-446655440001',
                    'date' => '2024-01-09',
                    'subject' => 'Bahasa Indonesia',
                    'summary' => null
                ],
                [
                    'id' => '550e8400-e29b-41d4-a716-446655440001',
                    'employee' => '550e8400-e29b-41d4-a716-446655440002',
                    'classroom' => '550e8400-e29b-41d4-a716-446655440000',
                    'date' => '2024-01-09',
                    'subject' => 'Bahasa Jepang',
                    'summary' => 'wajib datang'
                ]
            ]);

            JournalHour::insert([
                [
                    'journal' => '550e8400-e29b-41d4-a716-446655440000',
                    'hour' => '1'
                ],
                [
                    'journal' => '550e8400-e29b-41d4-a716-446655440001',
                    'hour' => '1'
                ],
                [
                    'journal' => '550e8400-e29b-41d4-a716-446655440000',
                    'hour' => '2'
                ],
            ]);
            DB::commit();
        } catch (\Throwable $th) {
            DB::rollBack();
            print($th->getMessage());
        }
    }
}
