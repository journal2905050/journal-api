<?php

namespace Database\Seeders\Data;

use App\Models\AcademicPeriode;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AcademicPeriodeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::beginTransaction();
        try {
            AcademicPeriode::create([
                'school_year' => '2023/2024',
                'is_active' => true,
            ]);
            DB::commit();
        } catch (\Throwable $th) {
            DB::rollBack();
            print($th->getMessage());
        }
    }
}
