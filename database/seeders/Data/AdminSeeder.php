<?php

namespace Database\Seeders\Data;

use App\Models\User;
use App\Models\People;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::beginTransaction();
        try {
            $person = People::create([
                'name' => 'Admin',
            ]);

            User::create([
                'id' => $person->id,
                'username' => 'admin@smk1ambarawa.com',
                'password' => Hash::make('12345678'),
                'is_admin' => true,
            ]);
            DB::commit();
        } catch (\Throwable $th) {
            DB::rollBack();
            print($th->getMessage());
        }
    }
}
