<?php

namespace Database\Seeders\Data;

use App\Models\User;
use App\Models\People;
use App\Models\Employee;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class EmployeeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::beginTransaction();
        try {
            $password = Hash::make('12345678');

            People::insert([
                [
                    'id' => '550e8400-e29b-41d4-a716-446655440002',
                    'name' => 'Budi Santoso',
                    'gender' => 'L',
                ],
                [
                    'id' => '550e8400-e29b-41d4-a716-446655440003',
                    'name' => 'Amelia Saraswati Dewi',
                    'gender' => 'P',
                ]
            ]);

            User::insert([
                [
                    'id' => '550e8400-e29b-41d4-a716-446655440002',
                    'username' => 'raja@smk1ambarawa.com',
                    'password' => $password,
                    'is_employee' => true,
                    'is_bk' => true,
                ]
            ]);

            User::insert([
                [
                    'id' => '550e8400-e29b-41d4-a716-446655440003',
                    'username' => 'lis@smk1ambarawa.com',
                    'password' => $password,
                    'is_bk' => true,
                    'is_admin' => true,
                ]
            ]);

            Employee::insert([
                [
                    'id' => '550e8400-e29b-41d4-a716-446655440002',
                    'alias' => 'BS',
                    'nip' => '04781',
                    'rank' => 'Pembina XII/PPLG 1',
                    'position' => 'Guru Bahasa Indonesia',
                    'status' => 'Guru ASN',
                ],
                [
                    'id' => '550e8400-e29b-41d4-a716-446655440003',
                    'alias' => 'ASD',
                    'nip' => '05201',
                    'rank' => 'Pembina XII/PPLG 2',
                    'position' => 'Guru Bahasa Jawa',
                    'status' => 'GTT',
                ],
            ]);
            DB::commit();
        } catch (\Throwable $th) {
            DB::rollBack();
            print($th->getMessage());
        }
    }
}
