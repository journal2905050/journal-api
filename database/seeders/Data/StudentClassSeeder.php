<?php

namespace Database\Seeders\Data;

use App\Models\StudentClass;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class StudentClassSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::beginTransaction();
        try {
            StudentClass::insert([
                [
                    'student' => '550e8400-e29b-41d4-a716-446655440001',
                    'classroom' => '550e8400-e29b-41d4-a716-446655440000'
                ],
                [
                    'student' => '550e8400-e29b-41d4-a716-446655440004',
                    'classroom' => '550e8400-e29b-41d4-a716-446655440001'
                ]
            ]);
            DB::commit();
        } catch (\Throwable $th) {
            DB::rollBack();
            print($th->getMessage());
        }
    }
}
