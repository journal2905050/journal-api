<?php

namespace Database\Seeders\Data;

use App\Models\Classroom;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class ClassroomSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::beginTransaction();
        try {
            Classroom::insert([
                [
                    'id' => '550e8400-e29b-41d4-a716-446655440000',
                    'school_year' => '2023/2024',
                    'grade' => '12',
                    'name' => 'PPLG 1'
                ],
                [
                    'id' => '550e8400-e29b-41d4-a716-446655440001',
                    'school_year' => '2023/2024',
                    'grade' => '12',
                    'name' => 'PPLG 2'
                ],
            ]);
            DB::commit();
        } catch (\Throwable $th) {
            DB::rollBack();
            print($th->getMessage());
        }
    }
}
