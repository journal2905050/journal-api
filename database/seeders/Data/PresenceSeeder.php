<?php

namespace Database\Seeders\Data;

use App\Models\Presence;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PresenceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::beginTransaction();
        try {
            Presence::insert([
                [
                    'journal' => '550e8400-e29b-41d4-a716-446655440000',
                    'student' => '550e8400-e29b-41d4-a716-446655440004',
                    'date' => '2024-01-09',
                    'hour' => '1',
                    'type' => 'M',
                    'note' => '',
                ]
            ]);
            DB::commit();
        } catch (\Throwable $th) {
            DB::rollBack();
            print($th->getMessage());
        }
    }
}
