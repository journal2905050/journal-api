<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('presences', function (Blueprint $table) {
            $table->id();

            $table->uuid('journal');
            $table->uuid('student');

            $table->date('date');
            $table->integer('hour');
            $table->string('type', 10)->nullable();
            $table->string('note')->nullable();

            $table->unique(['student', 'date', 'hour']);

            $table->foreign('journal')->on('journals')->references('id')->cascadeOnDelete()->cascadeOnUpdate();
            $table->foreign('student')->on('students')->references('id')->cascadeOnDelete()->cascadeOnUpdate();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('presences');
    }
};
