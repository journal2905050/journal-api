<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('journals', function (Blueprint $table) {
            $table->uuid('id')->primary();

            $table->uuid('employee');
            $table->uuid('classroom');

            $table->date('date');
            $table->string('subject');
            $table->string('summary')->nullable();

            $table->foreign('employee')->on('employees')->references('id')->cascadeOnDelete()->cascadeOnUpdate();
            $table->foreign('classroom')->on('classrooms')->references('id')->cascadeOnDelete()->cascadeOnUpdate();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('journals');
    }
};
